const mongoose =require("mongoose");

const recuiterSchema = new mongoose.Schema({
  userId : String,
  name : String,
  email :{type:String , unique :true},
  password: String,
  forgetKey:Number
});
// const authSchema = new mongoose.Schema({
//   googleToken:String,
//   userId: String,
//   email: String,
//   googleId: String
// });

const jobDetailsSchema = new mongoose.Schema({
  jobId : String,
  postedByUserId : String,
  jobTitle : String,
  email:String,
  organization:String,
  jobDescription:String,
  jobLocation:String,
  eligibilty:String,
  ctc:String,
  website:String ,
  jobRole:String,
  createdDate :Date
});

const applicantSchema = new mongoose.Schema({
  name : String,
  gender : String,
  phone : String,
  email : String,
  jobAppliedId : String ,
  createdDate :Date,
  jobTitle:String,
  uniqid:String,
  resumeId : String,
  status:String,    
});
const resumeSchema =new mongoose.Schema({
  resumeId: String,
  resume : Buffer
});
const dPSchema =new mongoose.Schema({
  userId :{type :String ,unique :true},
  url :String
});

const contactSchema=new mongoose.Schema({
  name : String,
  email : String,
  message: String,
});
module.exports={recuiterSchema :recuiterSchema,dPSchema:dPSchema,jobDetailsSchema:jobDetailsSchema,applicantSchema:applicantSchema,resumeSchema:resumeSchema,contactSchema:contactSchema};
