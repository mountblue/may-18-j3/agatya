const express =require("express");
const router = express.Router();
const bodyParser =require("body-parser");
const models =require("../model");
const cookieParser = require("cookie-parser");
const crypto = require("crypto-js");
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended :true}));
router.use(cookieParser());
router.post("/jobpost" , (req,res,next)=>{
  const data = {...req.body, createdDate : new Date()};

  if(data){
    const job =new models.JobDetailsModel(data);
    job.save((err)=>{
      if(err){
        console.error(err);
        res.status(400).send({message :"Job cannot be created ! Try Again"});
      }else{
        res.status(201).send({message :"job created succesfully"});
      }
    });
  }
});


module.exports =router;