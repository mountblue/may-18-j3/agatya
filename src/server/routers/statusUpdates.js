const express =require("express");
const router = express.Router();
const bodyParser =require("body-parser");
const models =require("../model");
const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "agatya2018@gmail.com",
    pass: "1q2w3e4r@123"
  }
});
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended :true}));

router.put("/statusUpdate" , (req,res,next)=>{
  const statusData ={...req.body};
  models.JobAppliedModel.findOne({uniqid:req.body.uid},function(err,data){
    const applicant =data.status;
    data.status = req.body.status;
    data.save((err)=>{
      if(err)
      {
        res.status(400).send({message :"Somehting went wrong! Please Try again"});
      }
      else
      {
        console.log("status Email send");
        res.status(201).send({message :"Status Updated Successfully"});
        const mailOptions = {
          from: "agatya2018@gmail.com",
          to: data.email,
          subject: "agatya -- Status Update",
          text: "Hi  "+data.name+"\n\n Your status has been updated from "+applicant+" to "+data.status+" . \nIf you need to contact us you can by visiting our home page.\n\n"
        };
              
        transporter.sendMail(mailOptions, function(error, info){
          if (error) {
           
          } else {
            
          }
        });
      }
    } );
  });   
});
module.exports =router;
