const express =require("express");
const router = express.Router();
const Cookies =require("cookies");
const bodyParser =require("body-parser");
const models =require("../model");
const cookieParser =require("cookie-parser");
const crypto = require("crypto-js");
router.use(cookieParser());
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended :true}));
const publicKey ="agatya";

async function getData(res ,req,user){
  const cookies = new Cookies(req,res);
  const email =user.email;
  let password ="";
  if(!user.token){
    const pass =user.password;
    const bytes =crypto.AES.decrypt(pass,publicKey);
    password =bytes.toString(crypto.enc.Utf8);
  }
  else{
    password=user.password;
  }
  await models.RecruiterModel.find({email :email ,password :password}).lean().exec((err ,data)=>{
    if(err|| data.length != 1){
      res.status(404).send({message : "Invalid Email or Password"});
    }else{
      const tokenObj = {email :data[0].email,password :data[0].password};
      const token = crypto.AES.encrypt(JSON.stringify(tokenObj) ,"privatekey");
      let c = delete data[0]["password"];
      let d=data;
      d[0]["token"] =token.toString();
      cookies.set("newtoken" ,token.toString() ,{maxAge :90000000 ,httpOnly :true});
      res.cookie = token.toString();
      res.status(200).send(d);
    }
  });
}

router.post("/login",async function(req,res,next){
  if(req.cookies.newtoken){
    const tokenString = req.cookies.newtoken;

    const tokenBytes =crypto.AES.decrypt(tokenString ,"privatekey");
    const tokenDecrypted = tokenBytes.toString(crypto.enc.Utf8);

    const jsonUser =JSON.parse(tokenDecrypted);
    jsonUser["token"] =true;
        
    getData(res,req,jsonUser);
  }else{ 

    if(req.body.email&&req.body.password){
      getData(res,req,req.body);
    }else{
      res.status(404).send({message : ""});
    }
  }
} 
);


module.exports =router;
