const express =require("express");
const router = express.Router();
const bodyParser =require("body-parser");
const models =require("../model");


router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended :true}));

router.get("/getstatus?:uniqid" , (req,res,next)=>{

  const uid= req.query.uniqid;
 
  models.JobAppliedModel.findOne({uniqid:uid},function(err,data){
    if(err)
    {
      res.status(200).send("Applicant Not find");
    }
    else{
      // const status =data.status;
      if(data){res.status(200).send(data);}else{res.status(200).send("Applicant Not find");}
    }
  });
});

module.exports =router;