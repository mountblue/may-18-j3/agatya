const express =require("express");
const router = express.Router();
const bodyParser =require("body-parser");
const models =require("../model");
const mongoose =require("mongoose");

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended :false}));

router.get("/updateJob?:id" ,(req,res,next)=>{
  const id = req.query.id;
  

  models.JobDetailsModel.find({"_id":mongoose.Types.ObjectId(id)} ,(err,data)=>{
    if(err){
      res.status(400).send({message :"Bad Request"});
    }else{
      res.status(200).send(data);
    }
  });

});
router.post("/updateJob" ,(req,res,next)=>{

  const data =req.body;
  
  models.JobDetailsModel.findOne({"_id" :mongoose.Types.ObjectId(data._id)}).exec((err ,nData)=>{
    nData._id=data._id;
    nData.organization=data.organization;
    nData.jobTitle=data.jobTitle;
    nData.postedByUserId=data.postedByUserId;
    nData.email=data.email;
    nData.jobDescription=data.jobDescription;
    nData.jobLocation=data.jobLocation;
    nData.eligibility=data.eligibility;
    nData.ctc=data.ctc;
    nData.website=data.website;
    nData.jobRole=data.jobRole;
    nData.save(err=>{
      if(err){
       
      }else{
        
      }
    });
  });
});
module.exports =  router;