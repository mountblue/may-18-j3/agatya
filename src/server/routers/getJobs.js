const express =require("express");
const router = express.Router();
const bodyParser =require("body-parser");
const models =require("../model");
const mongoose =require("mongoose");

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended :false}));

router.get("/getjob?:id" , (req,res,next)=>{
  const id = req.query.id;
  if(id){
    models.JobDetailsModel.find({"postedByUserId" : id} , (err,data)=>{
      if(err||!data){
        res.status(404).send({message :"No jobs are available at this time"});
      }else{
        res.status(200).send(data);
      }
    });
  }
  else{
    models.JobDetailsModel.find({} , (err,data)=>{
      if(err||!data){
        res.status(404).send({message :"No jobs are available at this time"});
      }else{
        res.status(200).send(data);
      }
    });
  }

});

router.get("/getjob/:keyword" , (req,res,next)=>{
  
  const keyword =req.params.keyword;
  if(keyword){
    models.JobDetailsModel.find({$or :[{"jobTitle" : {$regex : keyword ,$options :"i"}},{"organization" :{$regex :keyword ,$options :"i"}}]} ,(err ,data)=>{
      if(err || data.length<1){
        res.status(404).send({message :"No jobs found with this keyword ! Try other keywords"});
      }else{
        res.status(200).send(data);
      }
    });
  }
});
 

module.exports =router;