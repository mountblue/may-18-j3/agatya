const express =require("express");
const router = express.Router();
const bodyParser =require("body-parser");
const models =require("../model");

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended :true}));
const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "agatya2018@gmail.com",
    pass: "1q2w3e4r@123"
  }
});
router.post("/contactUs" , (req,res,next)=>{
  const formData ={...req.body};
  if(formData){
    const contactUs = models.ContactUsModel(req.body);
    //console.log(req.body);
    contactUs.name =req.body.name;
    contactUs.email =req.body.email;
    contactUs.message = req.body.message;
    contactUs.save((err)=>{
      if(err){
        res.status(400).send({message :"Somehting went wrong! Please Try again"});
      }else{
        res.status(201).send({message :"Thank you ! Your response is recorded  "});
        const mailOptions = {
          from: "agatya2018@gmail.com",
          to: req.body.email,
          subject: "agatya -- Contact",
          text: "Hi  "+req.body.name+"\n\n Thank you for contacting us. A representative will contact you soon. :)\n\n"+ "Team Agatya"
        };
        transporter.sendMail(mailOptions, function(error, info){
          if (error) {
          
          } else {
            
          }
        });
      }
    });
  }else{
    res.status(400).send({message :"Bad Request"});
  }
});

module.exports =router;
