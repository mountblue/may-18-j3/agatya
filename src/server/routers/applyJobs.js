const express =require("express");
const router = express.Router();
const bodyParser =require("body-parser");
const models =require("../model");
const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "agatya2018@gmail.com",
    pass: "1q2w3e4r@123"
  }
});
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended :true}));

router.post("/applyjob" , (req,res,next)=>{
  const formData ={...req.body,createdDate :new Date()};
  if(formData){
    const applicantApplied = models.JobAppliedModel(formData);
    applicantApplied.phone =formData.phone;
    applicantApplied.status = "Applied" ;
   
    applicantApplied.save((err)=>{
      if(err){
        res.status(400).send({message :"Somehting went wrong! Please Try again"});
      }else{
        res.status(201).send({message :"Job Applied succesfully"});
        const mailOptions = {
          from: "agatya2018@gmail.com",
          to: req.body.email,
          subject: "agatya -- Applied Job response",
          text: "Hi  "+req.body.name+"\n\n Thank you for applying to the job  "+req.body.jobTitle+" of id="+req.body.jobAppliedId+".\n Your unique ID is " +req.body.uniqid +". Please keep this ID for future reference.\n\n"+ "Team Agatya"
        };
              
        transporter.sendMail(mailOptions, function(error, info){
          if (error) {
           
          } else {
            
          }
        });
      }
    });
  }else{
    res.status(400).send({message :"Bad Request"});
  }
});

module.exports =router;

// The redirect URI in the request, http://13.127.171.252.nip.io:5000/auth/google/callback, does not match the ones authorized for the OAuth client. To update the authorized redirect URIs, visit: https://console.developers.google.com/apis/credentials/oauthclient/30252727810-2ajcqbk9liekfodu4379i8dvslrmrn38.apps.googleusercontent.com?project=30252727810

// Learn more

// Request Details
// response_type=code
// redirect_uri=http://13.127.171.252.nip.io:5000/auth/google/callback
// scope=profile email
// client_id=30252727810-2ajcqbk9liekfodu4379i8dvslrmrn38.apps.googleusercontent.com
// That’s all we know.