const express =require("express");
const router = express.Router();
const bodyParser =require("body-parser");
const models =require("../model");
const cookieParser =require("cookie-parser");
const crypto = require("crypto-js");
router.use(cookieParser());
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended :true}));

router.get("/logout" , function(req,res,next){
  if(req.cookies.newtoken){
    res.cookie("newtoken" ,"",{httpOnly :true});
    res.status(200).send({message :"Logged out successfully"});
  }else{
    res.status(404).send({message :"Login first!! You are not logged in"});
  }
});
module.exports =router;