const express =require("express");
const router = express.Router();
const bodyParser =require("body-parser");
const models =require("../model");
const mongoose =require("mongoose");

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended :false}));

router.get("/getapplicants?:id" ,(req,res,next)=>{
  const id = req.query.id;
  models.JobAppliedModel.find({"jobAppliedId" :id} ,(err ,data)=>{
    if(err){
      res.status(404).send({message : "No Applicant found"});
    }else{
      res.status(200).send({data : data});
    }
  });

});

module.exports =  router;