const router = require("express").Router();
const passport = require("passport");
const popupTools = require("popup-tools");
// // auth login
// router.get("/login", (req, res) => {
//   // res.render("login', { user: req.user });
//   res.send("go back and click on google");
// });

// // auth logout
// router.get("/logout", (req, res) => {
//   // handle with passport
//   req.logout();
//   res.redirect("https://accounts.google.com/logout");
// });

// auth with google+ | gmail*
router.get("/auth/google", passport.authenticate("google", {
  scope: ["profile", "email"]
}));

// router.get("/google", (req, res)=>{
//   console.log("in google");
// });

// callback route for google to redirect to
// hand control to passport to use code to grab profile info
router.get("/auth/google/callback", passport.authenticate("google"), function (req, res) {
  res.set({ "content-type": "text/html; charset=utf-8" });
  res.end(popupTools.popupResponse(req.user));
}
);

module.exports = router;
