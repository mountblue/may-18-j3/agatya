const mongoose =require("mongoose");
const schemas=require("./schema");

const RecruiterModel =mongoose.model("RecruiterData" , schemas.recuiterSchema);
const JobDetailsModel =mongoose.model("JobDetails" ,schemas.jobDetailsSchema);
const JobAppliedModel =mongoose.model("JobsApplied" ,schemas.applicantSchema);
const ResumeModel =mongoose.model("Resumes" ,schemas.resumeSchema);
const DPModel =mongoose.model("ProfilePic" ,schemas.dPSchema);
//............

//...............
const ContactUsModel =mongoose.model("contactus" ,schemas.contactSchema);

module.exports ={RecruiterModel:RecruiterModel,DPModel:DPModel,JobDetailsModel:JobDetailsModel ,JobAppliedModel:JobAppliedModel,ResumeModel:ResumeModel,ContactUsModel:ContactUsModel};
