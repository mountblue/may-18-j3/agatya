const express = require("express");
const app = express();
const expressWs=require("express-ws")(app);
const cookieParser = require("cookie-parser");
const cookieSession = require("cookie-session");
const bodyParser = require("body-parser");
const contactUsRouter=require("./routers/contactUs");
const loginRouter = require("./routers/loginRouter");
const signUpRouter = require("./routers/signUpRouter");
const postJobRouter = require("./routers/postJobRouter");
const applyJobRouter = require("./routers/applyJobs");
const getJobsRouter = require("./routers/getJobs");
const logoutRouter = require("./routers/logoutRouter");
//const  otpRouter = require("./routers/otpRouter");
const forgetPasswordRouter = require("./routers/forgetPasswordRouter");
const deleteJobRouter =require("./routers/deleteJobRouter");
const fetchApplicantsRouter = require("./routers/fetchApplicants");
const mongoose = require("mongoose");
const updateRouter = require("./routers/updateJobRouter");
const statusUpdatesRouter = require("./routers/statusUpdates");
const uploadRouter = require("./routers/uploadRouter");
const models = require("./model");
const statusRouter =require("./routers/getStatus");
const uri = require("./common");
const passport = require("passport");
const forgetRouter =require("./routers/newPWD");
mongoose.connect("mongodb://Sushant:1q2w3e4r@ds261460.mlab.com:61460/agatya", (err) => { if (err) { console.error(err); } });
const passportConfig = require("./passport-config/passport-google");

const Router = require("./routers/auth-routers");
const ProfileRouter=require("./routers/profile-routers");
const keys = require("../client/assets/keys");

/* app.use(cookieSession({
  maxAge: 24 * 60 * 60 * 1000,
  keys: [keys.seesion.cookieKey]
})); */
//initialize passport
app.use(passport.initialize());
app.use(passport.session());
//.......................................

app.use(cookieParser());
app.use(express.static("../../build"));
app.use(bodyParser.json({limit: "5mb"}));
app.use(bodyParser.urlencoded({limit: "5mb",extended: true}));
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://"+uri+":3000");
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, X-Custom-Header, Accept");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, XMODIFY");
  res.header("cookies_needed" ,true);
  next();
});
app.get("/auth/google",Router);
app.get("/auth/google/callback",Router);
app.get("/getjob" ,getJobsRouter);
app.get("/getjob/:keyword" ,getJobsRouter);
app.post("/jobpost" ,postJobRouter);
app.post("/signup" ,signUpRouter);
app.post("/login",loginRouter);
app.get("/logout",logoutRouter);
app.post("/applyjob" ,applyJobRouter);
app.post("/contactUs" ,contactUsRouter);
app.get("/getapplicants?:id" ,fetchApplicantsRouter);
app.post("/upload" ,uploadRouter);
app.put("/statusUpdate" ,statusUpdatesRouter);
app.get("/updateJob?:id" ,updateRouter);
app.post("/updateJob",updateRouter);
app.post("/forget",forgetPasswordRouter);
app.post("/forgetPwd" ,forgetRouter);
//app.post("/validate",otpRouter);
app.get("/getstatus?:uniqid" ,statusRouter);
app.get("/resume?:id" ,function(req,res,next){
  models.ResumeModel.findOne({resumeId:req.query.id} ,function(err,data){
    if(err){
      console.log(err);
    }else{
      res.send(data.resume);
    }
  });
});
app.get("/deletejob?:id",deleteJobRouter);
const aws =expressWs.getWss("/");

app.ws("/" ,(ws ,req)=>{
  ws.on("open" ,()=>{
  
    
    
  });
  ws.on("message" ,(msg)=>{
    const m =msg;
    aws.clients.forEach(function (client) {
      client.send(m);
    });
  });
});
app.ws("/fetchDP?:id" ,(ws,req)=>{
  const id =req.query.id;  
  models.DPModel.find({userId :id}).exec((err,data)=>{
    if(data[0]){
      ws.send(data[0].url);
    }
  });
  ws.on("message" ,(msg)=>{
    models.DPModel.find({userId :id}).exec((err,data)=>{
      if(data[0]){
        ws.send(data[0].url);
      }
    });
  });
});

app.ws("/searchapplicant?:id" ,(ws,req)=>{
  ws.on("message",(d)=>{
    const data =JSON.parse(d);
    let time =0;
    switch(data.time){
      case "past hour":
        time =1000*60*60;
        break;
      case "past 12 hour":
        time =1000*60*60*12;
        break;
      case "past day":
        time =1000*60*60*24;
        break;
      case "past week":
        time =1000*60*60*24*7;
        break;
      default:
      time =Date.now();
      }
      const requiredTime =new Date(Date.now()-time);
    models.JobAppliedModel.find({$and :[{"jobAppliedId" :data.id},{"createdDate":{$gte :requiredTime}} ,{$or :[{"name" :{$regex:data.msg, $options :"i"}},{"phone" :{$regex:data.msg, $options :"i"}},{"email" :{$regex:data.msg, $options :"i"}},{"status":{$regex:data.msg, $options :"i"}}]}]}).exec((err,da)=>{
      ws.send(JSON.stringify({data:da}));
    });
  });
});
app.ws("/filterapplicant" ,(ws ,req)=>{
  ws.on("message", (d)=>{
    const data =JSON.parse(d);
    let time=0;
    switch(data.msg){
    case "past hour":
      time =1000*60*60;
      break;
    case "past 12 hour":
      time =1000*60*60*12;
      break;
    case "past day":
      time =1000*60*60*24;
      break;
    case "past week":
      time =1000*60*60*24*7;
      break;
    default:
    }
    const requiredTime = new Date(Date.now()-time);
    models.JobAppliedModel.find({"jobAppliedId":data.id ,"createdDate":{$gte :requiredTime}}).exec((err,da)=>{
      ws.send(JSON.stringify(da));
    });
  });
});
app.ws("/uploadpic?:id" ,(ws,req)=>{
  const id =req.query.id;
  ws.on("message" ,(msg)=>{
    models.DPModel.findOne({userId :id}).exec((err,data)=>{
      if(err||!data){
        const obj =new models.DPModel({
          userId :id,
          url :msg
        });
        obj.save((err)=>{
          if(err) ws.send("upload failed");
        });

      }else{
        data.url =msg;
        data.save((err)=>{
          if(err) ws.send("upload failed");
        });
      }
    });
  });
});
app.listen(5000,function(err){
  console.log("5000");
});
