const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const keys = require("../../client/assets/keys");
const UserModel = require("../model");
const User = UserModel.RecruiterModel;
passport.serializeUser((user, done) => {
  /* here we get the user from the googlestratagy and we fuse it with
      mogodb id and push it to browser cokkie.... */
  //    console.log("sewrialize it");
  done(null, user.id);
});
passport.deserializeUser((id, done) => {
  /* here we get the user data(***mongo find***) for further linkage by above
     serializeUser */
  User.findById(id).then((data) => {
 
    done(null, data);
  });
});




passport.use(
  new GoogleStrategy({
    // options for google strategy
    clientID: keys.google.clientID,
    clientSecret: keys.google.clientSecret,
    callbackURL: "/auth/google/callback"
  }, (accessToken, refreshToken, profile, done) => {
    User.findOne({ email: profile.emails[0].value }).then((currentdata) => {
      if (currentdata) {
       
        //here we retrive the complete profile of the user if he was already there in database.....
        done(null, currentdata);
      }
      else {
        new User({
          password: accessToken,
          name: profile.displayName,
          email: profile.emails[0].value,
          userId: profile.id
        }).save().then((data) => {
         
          done(null, data);
        });
      }//we create a new user here if its was the first time....
    });

  })
);