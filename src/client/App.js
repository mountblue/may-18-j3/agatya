import React, { Component } from "react";
import "./css/App.css";
import Footer from "./components/footer";
import TermsAndPolicy from "./components/termsandpolicy";
import Login from "./components/login";
import Logo from "./components/logo";
import PostJob from "./components/postJob";
import RecruiterDash from "./components/recruiterDash";
import JobDesc from "./components/jobDesc";
import SignUp from "./components/signup";
import ApplyJob from "./components/applyjob";
import Error from "./components/Error";
import error from "./assets/error.png";
import ContactUs from "./components/contactus";
import ForgetPassword from "./components/forgetpassword";
import {authUser} from "./actions/authAction";
import JobRedirect  from "./components/jobRedirect";
import FAQ from "./components/faq";
import {BrowserRouter,Route,Switch} from "react-router-dom";
import ApplyRedirect from "./components/applyRedirect";
import AboutUs from "./components/aboutus";
import UpdateJob from "./components/updateJob";
import {connect} from "react-redux";
import JobUpdateRedirect from "./components/jobUpdateRedirect";

//import { forgetPassword } from "./actions/forgetPasswordAction";
//jobupdateredirect
function Redirect(matches){
  return (
    <React.Fragment><div style={{width:"100%",height:"500px",marginTop:"100px"}}><h1 style={{textAlign:"center",marginTop:"200px"}}>{matches.message}</h1></div>
    </React.Fragment>
  );
}
class App extends Component {
  state= {
    isLoggedin :false,
    currentURL :"/",
    displayError :"none"
  }
  componentWillMount(){
    this.props.authUser();
  }
  constructor(props){
    super(props);
    Object.prototype.iterator=(obj,callback)=>{
      if(typeof obj !="object"){
        throw Error("Only JSON objects allowed");
      }
      const keys = Object.keys(obj);
      keys.forEach(key=>{
        callback(key ,obj[key]);
      });
    };
  }
changeUrl=(url)=>{
  this.setState({
    currentURL :url
  });
}
  loginToggle=(bool)=>{
    if(bool=="false"){
      window.location.reload();
    }
    this.setState({
      isLoggedin :bool
    });
  }

  render() { 
    const l =this.state.isLoggedin?true:this.props.login?true:false;
    return ( 
      <BrowserRouter>
        <React.Fragment>
          <Error/>
          <Logo/>
          <Switch>
            <Route path ='/' exact strict
              render={()=>this.props.authReducer.isLoggedin===true?
                <RecruiterDash loginToggle={this.loginToggle}/>:<Login loginToggle={this.loginToggle} changeUrl ={this.changeUrl}/>}/>
            <Route path ='/signup' exact strict render={
              ()=>{
                return(
                  this.props.signupReducer.message?
                    <Redirect message ={this.props.signupReducer.message}/>:<SignUp/>);
              }}/>
            <Route path ='/jobpost' exact strict render={()=>this.props.authReducer.isLoggedin?
              <PostJob/>:<Login loginToggle={this.loginToggle} changeUrl ={this.changeUrl}/>}/>
            <Route path='/jobredirect' exact strict component={JobRedirect}/>
            <Route path='/aboutus' exact strict component={AboutUs}/>
            <Route path='/contactUs' exact strict component={ContactUs}/>
            <Route path='/forget' exact strict component={ForgetPassword}/>
            <Route path='/jobupdateredirect' exact strict component={JobUpdateRedirect}/>
            <Route path='/termsandpolicy' exact strict component={TermsAndPolicy}/>
            <Route path='/faq' exact strict component={FAQ}/>
            <Route path='/jobDesc/:id' component={JobDesc}/>
            <Route path='/updateJob' render ={
              ()=>{
                return(
                  !this.props.authReducer.isLoggedin?
                    <Login loginToggle={this.loginToggle} changeUrl ={this.changeUrl}/>: <UpdateJob/>);
              }
            }/>
            <Route path='/applyJobs/:id' component={this.props.applyJobReducer.message ?ApplyRedirect:ApplyJob}/>
            <Route render={
              ()=>{
             return (<React.Fragment><div style={{display:"grid",justifyItems:"center",marginTop:"150px",marginBottom:"90px"}}><img id="error" src={error} alt="Error"/><h2 style={{color :"red"}}>Page not found</h2><br/></div></React.Fragment>);
              }
            }/>
          </Switch>
          <Footer/>
        </React.Fragment>
      </BrowserRouter>
   
    );
  }
}
const mapStateToProps =state =>{
  return ({
    ...state,
    login: state.googleAuthReducer.data ? state.googleAuthReducer.data.googleId?true:false:false 
  });
 
};

export default connect(mapStateToProps,{authUser})(App);
//<SignUp/><RecruiterDash/><PostJob/><ApplyJob/>
