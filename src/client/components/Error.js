import React from "react";
import {connect} from "react-redux";
import {showError} from "../actions/errorAction";
const errorColor ="linear-gradient(to right bottom, #ff0000, #f74200, #f05d00, #e87104, #e1821b)";
const connectedColor ="linear-gradient(to right bottom, #178c1c, #56aa18, #8cc70e, #c3e401, #ffff00)";
class Error  extends React.Component{
    state ={
      message:""
    }
    
    componentWillReceiveProps(props){
      this.setState({
        message :this.props.errorReducer.message
      });
    }
    closeError=(e)=>{
      this.props.showError("");
    }
    render(){
      return (<div style ={{display :this.props.message?"block":"none",zIndex:"2",backgroundImage:this.props.message==="Connected again"?connectedColor:errorColor}} className='error-message'><p>{this.props.message?this.props.message:""}</p><span onClick={this.closeError} className='close-error'>&#x2715;</span></div>);
    }
}
const mapStateToStore =state =>{
  return {...state,
    message : state.errorReducer.message
  };
};
export default connect(mapStateToStore,{showError})(Error);
/*  */
