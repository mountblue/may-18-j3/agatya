import React from "react";
import "../css/App.css";
import "../css/formcss.css";
import DropDown from "./stateless/dropdownmenu";
import Input from "./stateless/input";
import TextArea from "./stateless/textArea";
import uniqid from "uniqid";
import firebaseInit from "./firebaseConfig";
import {connect} from "react-redux";
import {applyJobs} from "../actions/applyJobAction";
import {uploadResume} from "../actions/uploadAction";
import {showError} from "../actions/errorAction";
import {fetchAllJobs} from "../actions/getAllJObAction";
import { socket } from "../actions/webSocketAction";
class ApplyJob extends React.Component{
  componentWillMount(){
  // Initialize Firebase
    /* var config = {
      apiKey: "AIzaSyC3pfHLw_ZalIv7orEsbU_ZBztAnjfJozQ",
      authDomain: "agatya-97ce9.firebaseapp.com",
      databaseURL: "https://agatya-97ce9.firebaseio.com",
      projectId: "agatya-97ce9",
      storageBucket: "agatya-97ce9.appspot.com",
      messagingSenderId: "158864603114"
    };
    firebase.initializeApp(config); */
    this.props.fetchAllJobs({id :""});
  }
    handleClick=(e)=>{
      const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      const phoneformate = /^\d{10}$/;
      e.preventDefault();
      const mailCheck =e.target.elements.email.value.match(mailformat);
      const phoneCheck=e.target.elements.phone.value.match(phoneformate);
      if (mailCheck && phoneCheck) {
        const elements =e.target.elements;
        const name =elements.name.value;
        const phone =elements.phone.value;
        const email =elements.email.value;
        const address =elements.address.value;
        const gender =elements.gender.value;
        const url = this.props.url;
        const jobName =Array.isArray(this.props.jobs)?this.props.jobs.filter(job=>job._id==this.props.match.params.id)[0] :"";
        const applicantData={name, phone,email,gender ,address ,jobAppliedId :this.props.match.params.id,jobTitle :jobName.jobTitle,uniqid:uniqid("AGY-"),resumeId:url};
        socket.send(JSON.stringify({type:"applied",id :this.props.match.params.id}));
        this.props.applyJobs(applicantData);
      } 
      else {
        if(mailCheck)
          this.props.showError("You have entered an invalid phone number!");
        else if(phoneCheck)
          this.props.showError("You have entered an invalid Email-ID!");
        else
          this.props.showError("You have entered an invalid phone number & Email-ID!");
        return false;
      }
    }
    showLength=(e)=>{
      const file =e.target.files[0];
      const name =(new Date()).toString().replace(/ /g,"-")+file.name;
      if(file.type!=="application/pdf"){
        this.props.showError("Only pdf format resumes are allowed!!");
        return false;
      }
      if(file.size>1240000){
        this.props.showError("Resumes should be less than 1 MB");
        return false;
      }
      const storageRef =firebaseInit.storage().ref();
      const metadata = {"content-type": file.type};
      const task = storageRef.child(name).put(file,metadata);
      var bar=document.getElementById("myProgress");
      bar.style.display="block";
      var elem = document.getElementById("myBar");   
      var width = 1;
      var id = setInterval(frame, 20);
      function frame() {
        if (width >= 100) {
          clearInterval(id);
        } else {
          width++; 
          elem.style.width = width + "%"; 
          if(width==100){
            elem.style.display="none";
            document.getElementById("tick").style.display="block";
          }
        }
      }
      task.then(snapshot=>snapshot.ref.getDownloadURL()).then(url=>this.props.uploadResume(url));
 
    }
    render(){
        
      const jobName =Array.isArray(this.props.jobs)?this.props.jobs.filter(job=>job._id==this.props.match.params.id)[0]:{};
      const jobDetails =jobName?jobName:{};
      return(
        <form onSubmit={this.handleClick} className="FSJob">
          <div class="container" style={{marginTop:"100px"}}>
            <h1>Applying For</h1>
            <p>{jobDetails.jobTitle+" at "+jobDetails.organization}</p>
            <hr/>
            <Input name="name">Full Name</Input>
            <Input name="phone" place="Provide 10 Digit Mobile Number">Phone</Input>             
            <DropDown value={["Male","Female","Others"]} name="gender" type="gender">Gender</DropDown>
            <Input name="email">Email</Input>
            <TextArea name ='address' row={5} col={60}>Address</TextArea>
            <div class="upload-btn-wrapper">
              <button class="btn">Upload Resume</button>
              <input type="file" name="myfile" onChange={this.showLength}/>
              <div id="myProgress">
                <div id="myBar"></div>
                <h1 id="tick">&#x2713;</h1>
              </div>
            </div>
            <p>By Applying you agree to our <a style={{textDecoration:"underline"}} href="/termsandpolicy" >Terms & Privacy</a>.</p>
            <div className="clearfix">
              <button type="submit" className="apply">Apply Now!</button>
            </div></div>
        </form>
            
      );
    }
}
const mapStoreToProps =state =>{  return{
  ...state,
  jobs : state.getAllJobReducer.jobs,
  url :state.uploadReducer.url?state.uploadReducer.url:""
};};
export default connect( mapStoreToProps,{applyJobs,uploadResume,showError,fetchAllJobs})(ApplyJob);
