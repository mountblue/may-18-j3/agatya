import React from "react";
import {connect} from "react-redux";
import {authentication} from "../actions/authenticationAction";
const Loading =(props)=>{
  props.authentication();
  props.loginToggle(props.isLoggedIn);
  
  return (<h2 style={{marginTop:"100px"}}>Loading... !Please wait</h2>);
};
const mapStatetoProps=state=>({
  isLoggedIn :state.authReducer.isLoggedin
});

export default connect(mapStatetoProps ,{authentication})(Loading);