import React from "react";

function Number(props) {
  return (
    <React.Fragment>
      <label>{props.children}</label>
      <input type="number" placeholder={props.place} maxLength={props.len} name={props.name} className={props.class} />
      <br />
    </React.Fragment>
  );
}

export default Number;