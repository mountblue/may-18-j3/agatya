import React from "react";

function DropDown(props){
  
  return(
    <div style={{display :props.dis?props.dis:"block"}}>
      <label className={props.classN}>{props.children}</label>       
      <select id={props.type} style={props.style} className={props.classN} name={props.name} onChange ={props.ev}>
        <option></option>
        {props.value.map(function(elem,index){
          return <option value={elem}>{elem}</option>;
        })
        }
      </select>
      <br/>
    </div>
  );
}

export default DropDown;