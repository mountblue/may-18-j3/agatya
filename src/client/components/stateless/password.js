import React from "react";

function PassWord(props){
  return(
    <React.Fragment>
      <label>{props.children}</label>
      <input id="pass" type="password" placeholder={props.place} name={props.name} className={props.class} required/>
      <br />
    </React.Fragment>
  );
}

export default PassWord;