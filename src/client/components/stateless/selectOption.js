
import React from "react";

function SelectOption(props){
  let arrayStatus=props.value;
  arrayStatus.unshift(props.status);
  arrayStatus =Array.from(new Set(arrayStatus));
  return(
    <React.Fragment>       
      <select id={props.id} onChange={props.onChange}>
        {arrayStatus?arrayStatus.map(function(elem,index){
          return <option value={elem}>{elem}</option>;
        }):null
        }
      </select>
      <br/>
    </React.Fragment>
  );
}

export default SelectOption;