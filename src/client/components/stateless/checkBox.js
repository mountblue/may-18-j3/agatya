import React from "react";

function CheckBox(props){
  return(
    <React.Fragment>
      <span >{props.children}</span>
      <table >
        <tr>
          {
            props.value.map(function(elem){
              return <td><input type="checkbox"/><label>{elem}</label></td>;
            })
        
          }
        </tr>
      </table>
      <br/>
    </React.Fragment>
  );
}

export default CheckBox;