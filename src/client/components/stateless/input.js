import React from "react";

function Input(props){
  return(
    <React.Fragment>
      <label>{props.children}</label>
      <input type="text" placeholder={props.place} maxLength={props.len} name={props.name} className={props.class}/>
      <br />
    </React.Fragment>
  );
}

export default Input;