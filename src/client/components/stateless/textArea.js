import React from "react";

function TextArea(props){

  return(
    <React.Fragment>
      <label >{props.children}</label>
      <textarea  name={props.name} rows={props.row} cols={props.col}></textarea>
      <br/>
    </React.Fragment>
  );
}

export default TextArea;