import React, { Component } from "react";
import Adi from "../assets/adi.jpg";
import "../css/aboutcss.css";
import Mukund from "../assets/mukund.jpg";
import Sushant from "../assets/shushant.jpeg";
import Yogi from "../assets/yogi.jpg";

class AboutUs extends Component{

  render(){
    return(<React.Fragment>
      
      <div id="about" style={{marginTop:"100px"}}>
        <div>
          <h1 id="quote">“It is always the simple that produces the marvelous.” —Amelia Barr</h1>
        </div>
        
        <div>
          <h1 id="mission-head">-Our Mission-</h1>
          <p  id="mission-content">To provide the most actionable app for applicant tracking </p>
        </div>
        
        <div>
          <h1 id="about-head">-About Us-</h1>
          <p  id="about-content">Throughout the history of business, people use data to make more informed decisions. 
            Our mission at agatya is to make the app economy more transparent. Today we provide 
            the most actionable mobile app data & insights in the industry.
            We want to make this data available to as many people as possible (not just the top 5%).
          </p> 
        </div>
        
      
        <h1>-Team-</h1>
        <div id="team">
          <div className="teamMates">
            <img src={Adi} alt="Aditya"/>
            <h1 className="name">Aditya Kumar</h1>
            <h2>MERN Stack Developer</h2>
          </div>
          <div className="teamMates">
            <img src={Mukund} alt="Mukund" />
            <h1>Mukund Raj S T</h1>
            <h2>MERN Stack Developer</h2>
          </div>
        
          <div className="teamMates">
            <img src={Sushant} alt="Sushant" />
            <h1>Sushant Kumar</h1>
            <h2>MERN Stack Developer</h2>
          </div>
          <div className="teamMates">
            <img src={Yogi} alt="Yogi" />
            <h1>Yogendra Gupta</h1>
            <h2>MERN Stack Developer</h2>
          </div>
          
          
        </div>    
      </div>
      
    </React.Fragment>

    );
  }
}
export default AboutUs; 
