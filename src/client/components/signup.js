import React from "react";
import "../css/formcss.css";
import DropDown from "./stateless/dropdownmenu";
import {connect} from "react-redux";
import Input from "./stateless/input";
import PassWord from "./stateless/password";
import {publicKey} from "../assets/publickey";
import crypto from "crypto-js";
import {signupUser} from "../actions/signupAction";
import {Link} from "react-router-dom";
import {showError} from "../actions/errorAction";
class SignUp extends React.Component{
  myFunction = (e) => {
    
    var x = document.getElementById("pass");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
    handleClick=(e)=>{
      const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      const phoneformate = /^\d{10}$/;
      e.preventDefault();
      const checkEmail  =e.target.elements.email.value.match(mailformat);
      const checkPhone =e.target.elements.phone.value.match(phoneformate);
      if (checkPhone && checkEmail) {
        const elements =e.target.elements;
        const userdata ={name :elements.name.value ,phone :elements.phone.value ,email :elements.email.value ,gender :elements.gender.value ,password :crypto.AES.encrypt(elements.psw.value,publicKey).toString()};
        this.props.signupUser(userdata);
        //window.location.href='http://localhost:3000/signupredirect';
      }
      else {
        if(checkPhone)
          this.props.showError("You have entered an invalid Email-ID!");
        
        else if(checkEmail)
          this.props.showError("You have entered an invalid phone number!");
        else
          this.props.showError("You have entered an invalid Email-ID and phone number!");
            
        return false;
      }
      
      
    }
    
    render(){
      return(
        <form onSubmit={this.handleClick} className="FSJob">
          <div class="container" style={{marginTop:"100px"}}>
            <h1>Sign Up</h1>
            <p>Please fill in this form to create an account.</p>
            <hr/>
            <Input name="name">Name</Input>
            <Input name="phone">Phone</Input>
            <DropDown value={["Male","Female","Others"]} name="gender" type="gender">Gender</DropDown>
            <Input name="email">Email</Input>
            <PassWord  name="psw">Password</PassWord>
            <input type="checkbox" name="remember" onClick={this.myFunction} /> show password
            <p>By creating an account you agree to our <a style={{textDecoration:"underline"}} href="/termsandpolicy" >Terms & Privacy</a>.</p>
            <div className="clearfix">
              <Link to="/"><button onClick={this.validate} type="button" className="cancelbtn" style={{backgroundColor:"red"}}>Cancel</button></Link>
              {/*  <Link to={{ pathname: "/signupredirect", state: { message: this.props.message} }}> */}<button type="submit" className="signupbtn">Sign up</button>
                       
            </div></div>
        </form>
            
      );
    }
}
const mapStateToProps=state=>{
  return ({message : state.signupReducer.message});
};
export default connect(mapStateToProps,{signupUser,showError})(SignUp);
