import React from "react";
import { Link } from "react-router-dom";
import "../css/logincss.css";
import { connect } from "react-redux";
import { authUser } from "../actions/authAction";
import Cookie from "universal-cookie";
import { fetchAllJobs } from "../actions/getAllJObAction";
import crypto from "crypto-js";
import { publicKey } from "../assets/publickey";
import { googleAuth } from "../actions/googleAuthAction";
import { showError } from "../actions/errorAction";
import store from "../store";
import { uri } from "../common";
import logo from "../assets/logo.png";
import recr from "../assets/recr.jpg";
import appl from "../assets/appl.jpg";
import AnchorLink from "react-anchor-link-smooth-scroll";
const popupTools = require("popup-tools");

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = { message: this.props.message ,inputValue :"check-status" ,inputType :"button"};
  }
  componentWillMount() {
    this.props.authUser();
    this.props.fetchAllJobs({ id: "" });
  }
  myFunction = (e) => {

    const x = document.getElementById("pass");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
  checkStatus=(e)=>{
    e.preventDefault();
    this.setState({
      inputType:"text",
      inputValue:""
    });
  }
  changeButton=(e)=>{
    console.log("onfocuspot");
    e.preventDefault();
    this.setState({
      inputType:"button",
      inputValue :"check status"
    });
  }
  clicker = (e) => {
    const pr = this.props;
    popupTools.popup("http://13.127.171.252.nip.io:5000/auth/google", "google", { width: 800, height: 800 }, function (err, data) {
      // this executes when closed
      if (err) {

      } else {
        const d = data;
        store.dispatch(googleAuth(d));
        const pass = crypto.AES.encrypt(d.password, publicKey);
        pr.authUser({ email: d.email, password: pass.toString() });
      }
    });
  }

  handleClick = (e) => {
    e.preventDefault();
    const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (e.target.elements.uname.value.match(mailformat)) {
      const elements = e.target.elements;
      const pass = elements.psw.value;
      const encr = crypto.AES.encrypt(pass, publicKey);
      const loginCredential = { email: elements.uname.value, password: encr.toString() };
      this.props.authUser(loginCredential);
      this.props.loginToggle(this.props.isLoggedin);
      //document.form1.text1.focus();
      return true;
    }
    else {
      this.props.showError("You have entered an invalid email address!");
      return false;
    }
  }
  signup = () => {
    this.props.changeUrl("/signup");
  }
  // ValidateEmail=(inputText)=>{

  // }
  searchJobs = (e) => {
    this.props.fetchAllJobs({ id: e.target.value });
  }
  changeText=(e)=>{
    console.log(e);
    const text = e.target.value;
    this.setState({
      inputValue :text
    });
  }
  add=(e)=>{
    if(e.key==="Enter"){
      fetch("http://"+uri+":5000/getstatus?uniqid="+this.state.inputValue).then(response=>response.json())
        .then(data=>this.props.showError(`${data.name}!!  status for your job "${data.jobTitle}" is **${data.status}**... Thank You !! :)`)).catch(e=>this.props.showError("Something Went Wrong"));
    }
  }
    
  render() {
    this.props.loginToggle(this.props.isLoggedin);
    const allJobsData =this.props.allJobs;
    const jobSpan = Array.isArray(allJobsData)?allJobsData.sort((a,b)=>Date.parse(b.createdDate)-Date.parse(a.createdDate)).map(job=><section><Link to={"/jobDesc/"+job._id}><h1>{job.jobTitle}</h1></Link><p>{job.organization}</p><p>{job.ctc+" Lacs per annum"}</p></section>):<h3>Nothing found</h3>;
    return (
      <React.Fragment>
        <section id="banner">
          <div className="inner">
            <h1 className="logoname">agatya</h1>
            <img src={logo} alt="logo"/>
            <h2>Your Search Ends Here</h2>
            <div id="links" style={{display:"grid",justifyItems:"center",gridTemplateColumns:"1fr 1fr"}}>
              <AnchorLink  href='#one'>I am Recruiter</AnchorLink>
              <AnchorLink  href='#two'>I am Apllicant</AnchorLink>
            </div>
          </div>
        </section>
        <section className="inner-container">
          <article id="one" >
            <fieldset className="recr">
              <form onSubmit ={this.handleClick}>
                <h3 style ={{color :"red"}}>{this.props?this.props.message:""}</h3>           
                <h3 className="loginLabel">E-Mail</h3>
                <br/>
                <input type="text" placeholder="ex: abc@abc.com" name="uname" required className="loginbox"/> 
                <h3 className="loginLabel">Password</h3>
                <br/>
                <input type="password" id="pass" placeholder="Enter Password" name="psw" required className="loginbox"/>
                <label className="help">
                  <input className="showPwd"type="checkbox" name="remember" onClick={this.myFunction} />Show password
                </label>
                <button className="signUp">Sign In</button> 
                <Link to ="/forget">Need help?</Link>               
              </form>
              
              <p id="sigup">Do you want to create&nbsp;<Link to ="/signup">a new account?</Link></p>
              <h4 className="social">Or Login with  <button onClick={this.clicker} className="fa fa-google"></button></h4>
            </fieldset> 
            <div className="horizontal div1">
              <div className="vertical">
                <img src={recr}/>
                <p><i>"Success is where preparation and opportunity meet." </i></p>
              </div>
            </div>
           
          </article>

          <article id="two" >
            <div className="horizontal div1">
              <div className="vertical">
              
                <img src={appl}/>
                <p style={{textAlign:"center"}}> <i>"The more I read, the more certain I am that I know nothing."</i></p>
              </div>
            </div>
            <fieldset className="coll">
              <h1>Search</h1>
              <input id="status" type={this.state.inputType} onChange={this.changeText} onKeyDown={this.add} onClick={this.checkStatus} onBlur={this.changeButton} value={this.state.inputValue}/>
              <form method="post">
              
							  <input type="text" className="loginbox" placeholder="Search here" name="searchitem" onChange ={this.searchJobs}/>
                
                <div id="result">
                  {jobSpan}
							  </div>
              </form>
						  </fieldset> 
          </article>      
        </section>  
      </React.Fragment>);
  }
}
const mapStateToProps = state => {
  return {
    message: state.authReducer.data ? state.authReducer.data.message : "",
    isLoggedin: state.authReducer.isLoggedin,
    data: state.authReducer.data,
    signUpmessage: state.signupReducer.message,
    allJobs: state.getAllJobReducer.jobs

  };
};
export default connect(mapStateToProps, { authUser, fetchAllJobs, showError })(Login);
