import React from "react";
import "../css/formcss.css";
import InputValue from "./stateless/inputWithValue";
import {updateJob} from "../actions/updateJobAction";
import {authUser} from "../actions/authAction";
import {fetchJobs} from "../actions/getJobAction";
import {connect} from "react-redux";
import {showError} from "../actions/errorAction";
import {uri}  from "../common";
import { socket } from "../actions/webSocketAction";
class UpdateJob extends React.Component {
 
  componentWillMount(){
    this.props.authUser();
    const query =window.location.search;
    const id =query.substring(4,query.length);
    this.props.updateJob(id);
    
  }
 
    handleClick=(e)=>{
      e.preventDefault();
      const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      const ctcformate = /^\d*$/;
      e.preventDefault();
      const checkPhone =e.target.elements.email.value.match(mailformat);
      const checkCtc =e.target.elements.ctc.value.match(ctcformate);
      if ( checkPhone&&checkCtc ) {
        const _id=this.props.job._id;
        const organization = e.target.elements.organization.value;
        const email = e.target.elements.email.value;
        const website = e.target.elements.website.value;
        const ctc = e.target.elements.ctc.value;
        const jobTitle = e.target.elements.jobTitle.value;
        const jobDescription = e.target.elements.jobDescription.value;
        const eligibilty = e.target.elements.eligibilty.value;
        const jobLocation = e.target.elements.jobLocation.value;
        const jobRole =e.target.elements.jobRole.value;
        const postedBy= this.props.job.postedByUserId;
        this.props.updateJob({_id:_id,organization:organization,jobTitle :jobTitle,postedByUserId:postedBy,email:email,jobDescription:jobDescription,jobLocation:jobLocation,eligibilty:eligibilty,ctc:ctc,website:website ,jobRole:jobRole});
        socket.send("updated");
        setTimeout(()=>{window.location.href ="http://"+uri+":3000/jobupdateredirect";},500);
        
      }
      else {
        if(checkCtc)
          this.props.showError("You have entered an invalid phone number!");
        else if(checkPhone)
          this.props.showError("You have entered an invalid ctc");
        else
          this.props.showError("You have entered an invalid phone number and ctc!");
        //document.form1.text1.focus();
        return false;
      }
    }
    render() {
      const j =this.props.job;
      return (<form onSubmit={this.handleClick} className="FSJob" style={{marginTop:"100px"}}>
        <h1>Update Job</h1>
        <br/>
        <InputValue val={j.organization} name="organization">Employer/Organization Name</InputValue>
        <InputValue val={j.email} name ="email">Email Address</InputValue>
        <InputValue val={j.website} name="website">Employer Website</InputValue>
        <InputValue val={j.ctc} place="in LPA" name="ctc">CTC (in LPA)</InputValue>
        <InputValue val={j.jobTitle} name="jobTitle">Job Title</InputValue>
        <InputValue  val={j.jobDescription} row={5} col={60} name="jobDescription">Job Description</InputValue>
        <InputValue val={j.eligibilty} row={5} col={60} name="eligibilty">Eligibility/Qualifications</InputValue>
        <InputValue val={j.jobRole} row={5} col={60}name="jobRole">Roles and Responsibilities</InputValue>
        <InputValue val={j.jobLocation} name="jobLocation">Job Location (City & State)</InputValue>
        <br/>
        <input type="submit" name="Update" value="Update" className="submit_button" id="FSsubmit" />
      </form>);
    }}
const mapStateToProps = state =>{
  
  return {
    postedBy :state.authReducer.data?state.authReducer.data[0]._id:"",
    email : state.authReducer.data?state.authReducer.data[0].email:"" ,
    status :state.authReducer.ws   ,
    data:state.authReducer.data?state.authReducer.data[0]:"",
    job :Array.isArray(state.updateJobReducer.message)?state.updateJobReducer.message[0]:{}
  };};

export default  connect(mapStateToProps ,{updateJob,fetchJobs,showError,authUser})(UpdateJob);