import React, { Component } from "react";
import Input from "./stateless/input";
import TextArea from "./stateless/textArea";
import {connect} from "react-redux";
import {showError} from "../actions/errorAction";
import {contactUs} from "../actions/contactUsAction";

class ContactUs extends Component {
    handleClick=(e)=>{
      const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      e.preventDefault();
      const mailCheck =e.target.elements.email.value.match(mailformat);
      const nameCheck =e.target.elements.name.value;
      const messageCheck =e.target.elements.message.value;
      const elements =e.target.elements;
      const name =elements.name.value;
      const email =elements.email.value;
      const message =elements.message.value;
      if(nameCheck!="" && messageCheck!="" && mailCheck)
      {
        const contactData = {name,email,message};
        this.props.contactUs(contactData); 
        alert("Your response is recorded !");
        window.location.href="http://10.1.6.62:3000/";
      }
      else
      {
        this.props.showError("You have input invalid or empty Fields !");
        return false;
      }
    }
    render() {
      return (
        <form onSubmit={this.handleClick}  className="FSJob" style={{marginTop:"100px",height:"500px"}}>
          <h1>Contact US</h1>
          <Input name="name">Name</Input>
          <Input name="email">E-Mail</Input>
          <TextArea name="message">Message</TextArea>
          <button type="submit">Submit</button>
        </form>
      );
    }
}
  
export default connect(null,{contactUs,showError})(ContactUs);
