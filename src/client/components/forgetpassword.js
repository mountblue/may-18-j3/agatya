import React, { Component } from "react";
import Input from "./stateless/input";
import {connect} from "react-redux";
import {showError} from "../actions/errorAction";
import PassWord from "./stateless/password";

class ForgetPassword extends Component {
    state ={
      otp :"",
      email:"",
      display :"none",
      display1 :"block",
      display2 :"none",
    }
    handleClick=(e)=>{
      e.preventDefault();
      const elements =e.target.elements;
      const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      const email =elements.email.value;     
      const mailCheck =email.match(mailformat);

      if(mailCheck){
        const maildata = {email};
        fetch("http://10.1.6.62:5000/forget" ,{
          method :"POST",
          headers : {
            "content-type" : "application/json"
          },
          body :JSON.stringify(maildata)
        }).then(response => {
          return response.json();
        }).then(otp=>{ 
          this.setState({otp :otp.forgetKey,email:otp.email,display:"block" ,display1 :"none"});
            
        }); 
        
      } 
      else { 
        this.props.showError("This E-mail id not valid!");
        return false;
      } 
   
    }
    otpClick = (e)=>
    {
      e.preventDefault();
      const OTP1 =+e.target.elements.otp.value;
      const OTP2=this.state.otp;
      if(OTP1===OTP2)
      {
        this.setState({display2:"block" ,display :"none"});
      }else{
        this.props.showError("Wrong OTP please Try again");
      }
    }
    newPWDClick = (e)=>
    {
        e.preventDefault();
      const PWD1=e.target.PWD1.value; 
      const PWD2=e.target.PWD2.value;
      if(PWD1===PWD2)
      {
        const pwd={email :this.state.email,password:PWD1};
        fetch("http://10.1.6.62:5000/forgetPwd" ,{
          method :"POST",
          headers : {
            "content-type" : "application/json"
          },
          body :JSON.stringify(pwd)
        }).then(response => {
          return response.json();
        }).then(data=>{ 
          console.log(data);
          window.location.href ="http://10.1.6.62:3000";
        }); 
      }else
      {
        this.props.showError("password not confirmed");
            
      }
    }
    render() {
      return (
        <div>
          <form onSubmit={this.handleClick}  className="FSJob" style={{display :this.state.display1,marginTop:"100px"}}  >
            <h1>Forget Password</h1>
            <Input name="email">E-mail</Input>
            <button type="submit">Submit</button>
          </form>
          <form onSubmit={this.otpClick} id="otp"  className="FSJob"  style={{display :this.state.display,marginTop:"100px"}}>
            <h1>OTP</h1>
            <Input name="otp"></Input>
            <button type="submit">OK</button>
          </form>
          <form onSubmit={this.newPWDClick} id="otp"  className="FSJob"  style={{display :this.state.display2,marginTop:"100px"}}>
            <h1></h1>
            <PassWord name="PWD1">New Password</PassWord>
            <PassWord name="PWD2">Confirm Password</PassWord>
            <button type="submit">OK</button>
          </form>
        </div>
      );
    }
}
  
export default connect(null,{showError})(ForgetPassword);