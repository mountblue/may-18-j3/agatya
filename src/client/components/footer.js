import React, { Component } from "react";
import "../css/footercss.css";
import logo from "../assets/logo.png";

class Footer extends Component {

  render() {
    return (  <footer style={{marginTop:"10px"}} className="footer-basic-centered">

      <p class="footer-company-motto" id="motto">Your Search Ends Here</p>

      <p class="footer-company-name">agatya &copy; 2018</p>

    </footer> );
  }
}
  
export default Footer;
