import React from "react";
import {connect} from "react-redux";
import home from "../assets/home.png";
import {showError} from "../actions/errorAction";

const SignUpRedirect=(props)=>{
  return (
    <React.Fragment style={{marginTop:"100px"}}><h2>{!props.signupReducer.message?"Signup successful":"Signup failed Try Again"}</h2>
      <a href="/"><img id="home" src={home} alt ="Home"/></a></React.Fragment>
  );
};
const mapStateToProps =state =>({...state});
export default connect(mapStateToProps,{showError})(SignUpRedirect);
