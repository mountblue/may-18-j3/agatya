import React from "react";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {showError} from "../actions/errorAction";
class JobDesc extends React.Component{
    
  render(){
    const id =  this.props.match.params.id;
    const jobDetails = Array.isArray(this.props.state.getAllJobReducer.jobs)?this.props.state.getAllJobReducer.jobs.filter(job=>id===job._id):{};        
    
    return(
      <React.Fragment>
        <div id="jd" style={{marginTop:"100px",height:"500px",width:"80%",textAlign:"center"}}>
          <h1>{jobDetails[0].jobTitle}</h1>
          <h2>at {jobDetails[0].organization}</h2>
          <h3>CTC: {jobDetails[0].ctc} LPA </h3>
          <p>Location- {jobDetails[0].jobLocation}</p>
          <p>Job Description: <br/>{jobDetails[0].jobDescription}</p>
          <p>Job Roles & Responsibilties: <br/>{jobDetails[0].jobRole}</p>
          <p>Eligibilty Criteria: <br/>{jobDetails[0].eligibilty}</p>
          <Link to={"/applyJobs/"+this.props.match.params.id}><button className="apply">Apply Now!</button></Link></div>
      </React.Fragment>
    );
  }
}
const MapStateToProps =state =>({
  state :state
});
export default connect(MapStateToProps ,{showError})(JobDesc);
