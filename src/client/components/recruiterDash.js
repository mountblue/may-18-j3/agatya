import React, { Component } from "react";
import "../css/recruiterDashcss.css";
import {connect} from "react-redux";
import {logoutUser} from "../actions/logoutAction";
import {fetchJobs} from "../actions/getJobAction";
import {getApplicants} from  "../actions/fetchApplicantsAction";
import SelectOption from  "./stateless/selectOption";
import FileSaver from "file-saver";
import {showError} from "../actions/errorAction";
import {socket} from "../actions/webSocketAction";
import {deleteJob} from "../actions/deleteJobAction";
import {uri} from "../common";
import user from "../assets/user.png";
import {Link} from "react-router-dom";
import {updateJob} from "../actions/updateJobAction";
import DropDown from "./stateless/dropdownmenu";
import $ from "jquery";
import store from "../store";
import firebaseInit from "./firebaseConfig";
import { FETCH_APPLICANTS } from "../actions/types";
 
let newSocket;
let filterSocket;
let uploadSocket;
let fetchSocket;
class RecruiterDash extends Component{
  state ={
    display :"none",
    quote :"",
    showJobId :"",
    imageURL :user
  }
  componentDidMount(){
    $("#propic").click(function(e) {
      $("#dp").click();
    });
  }
  
  componentWillMount(){
    newSocket =new WebSocket("ws://"+uri+":5000/searchapplicant?id="+this.props.id);
    filterSocket =new WebSocket("ws://"+uri+":5000/filterapplicant?id="+this.props.id);
    uploadSocket =new WebSocket("ws://"+uri+":5000/uploadpic?id="+this.props.id);
    fetchSocket =new WebSocket("ws://"+uri+":5000/fetchDP?id="+this.props.id);
    fetchSocket.onmessage =(ev)=>{
if(ev.data){     
 this.setState({
        imageURL :ev.data
      });}
    };
    newSocket.onmessage =(ev)=>{
try{     
 const data =JSON.parse(ev.data);
      store.dispatch({type : FETCH_APPLICANTS ,
        payload :data
      });
}catch(e){}
    };
    uploadSocket.onmessage =(ev)=>{
     
    };
    filterSocket.onmessage =(ev)=>{
try{     
 const data =JSON.parse(ev.data);
    
  store.dispatch({
        type :FETCH_APPLICANTS,
        payload :data
      });}catch(e){}
      // store.dispatch({type : FETCH_APPLICANTS ,
      //   payload :data
      // })
    };
    this.props.fetchJobs({type :"query" ,id :this.props.id});
    fetch("https://talaikis.com/api/quotes/random/").then(data=>data.json()

    ).then(q=>{
      this.setState({
        quote:q.quote
      });
    });
  }
    uploadPic=(e)=>{
      const file =e.target.files[0];
      if(file){const storageRef =firebaseInit.storage().ref();
        const metadata = {"content-type": file.type};
        const task = storageRef.child(new Date()+"pic").put(file,metadata);
        task.then(snapshot=>snapshot.ref.getDownloadURL()).then(url=>{
          uploadSocket.send(url);
          this.setState({
            imageURL :url
          });
        });}
    }
  
    tableShow=(e)=>{
      e.preventDefault();
      const id =e.target.parentElement.id;
      const tab=document.getElementById("feed");
      const search=document.getElementById("srch");
      document.getElementById("quote").style.display="none";
      tab.style.display="block";
      search.style.display="block";
      this.setState({showJobId :id});
      this.props.getApplicants(id);
      
    }
   logoutClick=(e)=>{
     this.props.logoutUser();
     setTimeout(()=>{this.props.loginToggle("false");},600);
   }
   updateJob=(e)=>{
     
   }
   deleteJob=(e)=>{
     if(window.confirm("Are You sure you want to delete?\n")){
       this.props.deleteJob(e.target.parentElement.id);
       socket.send(JSON.stringify({type:"deleted",id:""}));
       setTimeout(()=>{window.location.href="http://"+uri+":3000/";},500);
     }          
     
   } 
   downloadResume=(e)=>{
     const id =e.target.id;
     fetch("http://"+uri+":5000/resume?id="+id).then(response=>response.arrayBuffer()).then(buff =>{
       const data =new Int8Array(buff);
       const blob =new Blob([data],{type :"octet/stream"});
     });
     const blob =new Blob([91,111,98,106,101,99,116,32,79,98,106,101,99,116,93] ,);

   }
   changeStatus=(e)=>{
     const uniqid = e.target.id;
     const status = e.target.value;     
     this.props.applicantsData.map((data)=>{ if(data.uniqid==uniqid){data.status=status;}});
     fetch("http://"+uri+":5000/statusUpdate", {
       method: "PUT",
       headers :{
         "content-type" :"application/json"
       },
       body:JSON.stringify({status:status,uid:uniqid}),
     })
       .then(response => response.json()).then(d=>{})
       .catch(error => console.error("Error:", error));
   }
   searchApplicant=(e)=>{
     const m =e.target.value;
     const flt = document.getElementById("filter");
     newSocket.send(JSON.stringify({time :flt.value ,id:this.state.showJobId,msg :m}));
   }
   filterApplicant=(e)=>{
     if(e.target.value){
       filterSocket.send(JSON.stringify({id :this.state.showJobId ,msg :e.target.value}));
     }else{
       this.props.getApplicants(this.state.showJobId);
     }
   }
   download=()=>{
     const data = this.props.applicantsData.map(details=>details);
     if(!data[0]._id){
       this.props.showError("No applicants Data to download");
       return false;
     }  
     let inputJsons = data;
     inputJsons.map(data=> delete data.resumeId);
     inputJsons.map(data=> delete data.__v);
     let key = Object.keys(inputJsons[0]);
     let fileData = inputJsons.map(data => Object.values(data));  
     let rows = "";
     for(let i=0;i<key.length;i++)
     {
       if(i<key.length-1)
       {
         rows = rows + key[i]+",";
       }
       else{
         rows = rows +key[i]+"\n";
       }
     }
     fileData.map((Data)=>
     {  
       return  Data.map((datas,j)=>
       {
         if(j<Data.length-1)
           return rows = rows +datas+",";
         else
           return rows = rows +datas+"\n";
       });
     });
     var blob = new Blob([rows], {type: "text/plain;charset=utf-8"});
     FileSaver.saveAs( blob,"applicant.csv"); 
   }


   render(){
     const arr =this.props.jobs;
     const applicantsData =this.props.applicantsData;
     applicantsData.sort((a,b)=>{
       return Date.parse(b.createdDate)-Date.parse(a.createdDate);
     });
     const applicantSpan = Object.keys(applicantsData[0]).length !== 0?applicantsData.map(applicant => (<React.Fragment><p><strong>Name:&nbsp;</strong>{applicant.name}<br/><strong>Email:&nbsp;</strong>{applicant.email}<br/><strong>Phone:&nbsp;</strong>{applicant.phone}<br/><strong>Created Date:&nbsp;</strong>{new Date(Date.parse(applicant.createdDate)).toLocaleString()}<br/><strong>Status:&nbsp;</strong><SelectOption id={applicant.uniqid} onChange={this.changeStatus} status={applicant.status} value={["Applied","Selected","Rejected","Interview"]}/><br/><strong>Resume:&nbsp;</strong><a target="_blank" href={applicant.resumeId}>{applicant.resumeId?"Download Resume":"No resume"}</a></p><hr/></React.Fragment>)) :[];
     const tableJobSpan = arr.map(job=>(<li className="tableData" id={job._id}><span id="jobName" onClick ={this.tableShow}>{job.jobTitle}</span><span onClick={this.updateJob} style={{textDecoration:"none"}}><Link to={"/updateJob?id="+job._id}>&#x270E;</Link></span><span onClick={this.deleteJob}>&#x2715;</span></li>));
     return(
       <React.Fragment>
         

         <div style={{height:"700px"}}>
           
           <div className="navbar" >
             <img src={this.state.imageURL} alt="user" id="propic" style={{marginTop:"70px"}}/>
             <input type="file" id="dp" onChange={this.uploadPic}/>
             <div className="jobsBoard">
               <h1 className="profileName" >
                 {this.props.name?"  "+this.props.name:""}
               </h1>
               <h2 id="header" >Previous jobs posted</h2>
               <ul className="jobs" id="jobs">               
                 {tableJobSpan}
               </ul >
             </div><hr style={{width:"70%"}}/>
             <button className="pj"><a href="/jobpost">Post Job</a></button>
             <button onClick={this.download} >Download Applicants Details</button>
             <button className ="logout-button" onClick={this.logoutClick}>Logout</button><br/>
             <select id="filter" style={{marginRight:"5%",marginLeft:"5%",width:"90%"}} onChange={this.filterApplicant}>
               <option value="" >--select--</option>
               <option value="past hour" >past hour</option>
               <option value="past 12 hour" >past 12 hour</option>
               <option value="past day" >past day</option>
               <option value="past week" >past week</option>
             </select>
           </div>
             
           <div className="Dash" style={{marginTop:"100px",marginLeft:"310px"}}>
             <h1 id="quote">{"\""+this.state.quote+"..\""}</h1>
             <h2 id ="xjob">{Array.isArray(this.props.applicantsData)?this.props.applicantsData[0].jobTitle:""}</h2>
             <input type="text" placeholder="Search here" id="srch" name="search" onChange={this.searchApplicant} style={{padding:"10px",display:"none",width:"100%"}}/>
             <div className="feeds" style={{display:"none",padding:"10px",width:"100%",height:"400px",overflow:"auto",backgroundColor:"rgba(255,255,255,0.3)"}} id="feed">
                
               {applicantSpan[0]?applicantSpan:<h1 style={{marginTop:"18%",textAlign:"center"}}>"Nothing found"</h1>}
             </div>
           </div>
         </div>
         
       </React.Fragment>
     );
   }
}
const mapStateToProps = state =>{
  return {
    jobs :state.getJobReducer.jobs,
    name :Array.isArray(state.authReducer.data)?state.authReducer.data[0].name:"",
    id : state.authReducer.data[0]._id,
    applicantsData : Array.isArray(state.fetchApplicantReducer.applicantsData.data)?state.fetchApplicantReducer.applicantsData.data[0]?state.fetchApplicantReducer.applicantsData.data:[{}]:Array.isArray(state.fetchApplicantReducer.applicantsData)?state.fetchApplicantReducer.applicantsData[0]?state.fetchApplicantReducer.applicantsData:[{}]:[{}],
  };
};
export default connect(mapStateToProps ,{updateJob,fetchJobs,logoutUser,getApplicants,showError,deleteJob})(RecruiterDash);
