import React from "react";
import "../css/formcss.css";

import Input from "./stateless/input";
import TextArea from "./stateless/textArea";
import DropDown from "./stateless/dropdownmenu";
import CheckBox from "./stateless/checkBox";
import {postJobs} from "../actions/postJobAction";
import {connect} from "react-redux";
import {showError} from "../actions/errorAction";
import {uri}  from "../common";
import {socket}from "../actions/webSocketAction";
class PostJob extends React.Component {
    handleClick=(e)=>{
      e.preventDefault();
      const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      const ctcformate = /^\d*$/;
      e.preventDefault();
      const checkPhone =e.target.elements.email.value.match(mailformat);
      const checkCtc =e.target.elements.ctc.value.match(ctcformate);
      if ( checkPhone&&checkCtc ) {
        const organization = e.target.elements.organization.value;
        const email = e.target.elements.email.value;
        const website = e.target.elements.website.value;
        const ctc = e.target.elements.ctc.value;
        const jobTitle = e.target.elements.jobTitle.value;
        const jobDescription = e.target.elements.jobDescription.value;
        const eligibilty = e.target.elements.eligibilty.value;
        const jobLocation = e.target.elements.jobLocation.value;
        const jobRole =e.target.elements.jobRole.value;
        const postedBy= this.props.postedBy;
        this.props.postJobs({organization:organization,jobTitle :jobTitle,postedByUserId:postedBy,email:email,jobDescription:jobDescription,jobLocation:jobLocation,eligibilty:eligibilty,ctc:ctc,website:website ,jobRole:jobRole});
        socket.send(JSON.stringify({type :"added" , id :postedBy}));
        setTimeout(()=>{window.location.href ="http://"+uri+":3000/jobredirect"; },1000); 
      }
      else {
        if(checkCtc)
          this.props.showError("You have entered an invalid phone number!");
        else if(checkPhone)
          this.props.showError("You have entered an invalid ctc");
        else
          this.props.showError("You have entered an invalid phone number and ctc!");
        //document.form1.text1.focus();
        return false;
      }
    }
    render() {
      return (<form onSubmit={this.handleClick} className="FSJob post" style={{marginTop:"100px"}}>
        <h1 id="poster">Post Job</h1>
        <p>Fill fields relevant to your needs!</p>
        <br />
        <Input name="organization">Employer/Organization Name</Input>
        <Input name ="email">Email Address</Input>
        <Input name="website">Employer Website</Input>
        <Input place="in LPA" name="ctc">CTC (in LPA)</Input>
        <Input name="jobTitle">Job Title</Input>
        <TextArea row={5} col={60} name="jobDescription">Job Description</TextArea>
        <TextArea row={5} col={60} name="eligibilty">Eligibility/Qualifications</TextArea>
        <TextArea row={5} col={60}name="jobRole">Roles and Responsibilities</TextArea>
        <Input name="jobLocation">Job Location (City & State)</Input>
        <br/>
        <input type="submit" name="Submit" value="Submit" className="submit_button" id="FSsubmit" />
      </form>);
    }}
const mapStateToProps = state =>({
  postedBy: state.authReducer.data ? state.authReducer.data[0]._id : state.googleAuthReducer.data ? state.googleAuthReducer.data._id:"",
  email: state.authReducer.data ? state.authReducer.data[0].email : state.googleAuthReducer.data ? state.googleAuthReducer.data.email : "",    
  status: state.authReducer.ws  
}
);

export default  connect(mapStateToProps ,{postJobs,showError})(PostJob);
