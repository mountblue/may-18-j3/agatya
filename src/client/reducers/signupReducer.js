import  {SIGNUP_USER} from "../actions/types";

const initialState ={};
export default function(state =initialState ,actions){
  if(actions.type===SIGNUP_USER){
    return {
      ...state ,
      message :actions.payload.message
    };
  }else{
    return state;
  }
}