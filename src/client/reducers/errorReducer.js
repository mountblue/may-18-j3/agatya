import  {ERROR} from "../actions/types";

const initialState ={
  message :""
};
export default function(state =initialState ,actions){
  if(actions.type==ERROR){
    return{
      ...state,
      message :actions.payload
    };
  }else{
    return state;
  }
}