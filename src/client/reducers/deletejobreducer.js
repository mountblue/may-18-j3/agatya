import  {DELETE_JOB} from "../actions/types";

const initialState ={
  message :""
};
export default function(state =initialState ,actions){
  if(actions.type==DELETE_JOB){
    return{
      ...state,
      message :actions.payload
    };
  }else{
    return state;
  }
}