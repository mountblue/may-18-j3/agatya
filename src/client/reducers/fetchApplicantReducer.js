import  {FETCH_APPLICANTS} from "../actions/types";

const initialState ={
  applicantsData :[]
};
export default function(state =initialState ,actions){
  if(actions.type==FETCH_APPLICANTS){
    return{
      ...state,
      applicantsData :actions.payload
    };
  }else{
    return state;
  }
}