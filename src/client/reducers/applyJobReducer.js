import  {APPLY_JOBS} from "../actions/types";

const initialState ={
  message :""
};
export default function(state =initialState ,actions){
  if(actions.type==APPLY_JOBS){
    return{
      ...state,
      message :actions.payload.message
    };
  }else{
    return state;
  }
}