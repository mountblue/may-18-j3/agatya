import  {FETCH_ALL_JOBS} from "../actions/types";

const initialState ={
  jobs :[]
};
export default function(state =initialState ,actions){
  if(actions.type==FETCH_ALL_JOBS){
    return {
      ...state,
      jobs : actions.payload
    };
  }else{
    return state;
  }
}