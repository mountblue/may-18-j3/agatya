import { combineReducers } from "redux";
import authReducer from "./authReducer";
import postJobReducer from "./postJobReducer";
import getJobReducer from "./getJobReducer";
import signupReducer from "./signupReducer";
import applyJobReducer from "./applyJobReducer";
import googleAuthReducer from "./googleAuthReducer";
import getAllJobReducer from "./getAllJobReducer";
import fetchApplicantReducer from "./fetchApplicantReducer";
import uploadReducer from "./uploadReducer";
import errorReducer from "./errorReducer";
import updateJobReducer from "./updateJobReducer";
export default combineReducers({
  authReducer:authReducer,
  postJobReducer :postJobReducer,
  getJobReducer : getJobReducer,
  signupReducer :signupReducer,
  applyJobReducer : applyJobReducer,
  getAllJobReducer :getAllJobReducer,
  fetchApplicantReducer:fetchApplicantReducer,
  uploadReducer : uploadReducer,
  errorReducer:errorReducer,
  googleAuthReducer: googleAuthReducer,
  updateJobReducer:updateJobReducer
});