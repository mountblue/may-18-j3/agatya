import  {AUTH_USER} from "../actions/types";
import {fetchAllJobs} from "../actions/getAllJObAction";
const initialState ={
  isLoggedin :false
};
export default function(state =initialState ,actions){
  if(actions.type===AUTH_USER){

    const newState = Object.assign({},state);
    if(actions.payload){
      newState.isLoggedin =actions.payload[0]?true:actions.payload.googleId?true:false;
      
    }
    return {
      ...newState,
      data : actions.payload
    };
  }else{
    return state;
  }
}
