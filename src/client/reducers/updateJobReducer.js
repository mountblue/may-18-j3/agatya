import  {UPDATE_JOB} from "../actions/types";

const initialState ={
  message :""
};
export default function(state =initialState ,actions){
  
  if(actions.type==UPDATE_JOB){
    return{
      ...state,
      message :actions.payload
    };
  }else{
    return state;
  }
}