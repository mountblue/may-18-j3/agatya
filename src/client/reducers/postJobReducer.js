import  {POST_JOBS} from "../actions/types";

const initialState ={
  jobdetails :[]
};
export default function(state =initialState ,actions){
  if(actions.type==POST_JOBS){
    return {
      ...state,
      jobdetails : actions.payload
    };
  }else{
    return state;
  }
}