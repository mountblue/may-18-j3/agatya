import  {UPLOAD_RESUME} from "../actions/types";

const initialState ={};
export default function(state =initialState ,actions){
  if(actions.type==UPLOAD_RESUME){
    return {
      ...state ,
      url :actions.payload
    };
  }else{
    return state;
  }
}