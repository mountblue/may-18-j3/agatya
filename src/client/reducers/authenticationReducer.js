import  {AUTHENTICATION} from "../actions/types";

const initialState ={
  isLoggedin :false
};
export default function(state =initialState ,actions){
  if(actions.type==AUTHENTICATION){

    const newState = Object.assign({},state);
    if(actions.payload){
            
      newState.isLoggedin =actions.payload?true:false;
    }
    return {
      ...newState,
      data : actions.payload
    };
  }else{
    return state;
  }
}