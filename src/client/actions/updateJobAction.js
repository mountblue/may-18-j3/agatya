import {UPDATE_JOB} from "./types";
import {uri} from "../common";
export const updateJob=(jobId)=> dispatch=>{
  if(typeof jobId === "string"){
    fetch("http://"+uri+":5000/updateJob?id="+jobId ,{
      method :"GET",
      credentials:"include",
      headers : {
        "content-type" : "application/json"
      }
    }).then(response => {
      return response.json();
    }).then(data=>{
     
      dispatch({
        type : UPDATE_JOB,
        payload :data
      });
    }
    );   
  }else{
    fetch("http://"+uri+":5000/updateJob" ,{
      method :"POST",
      credentials:"include",
      headers : {
        "content-type" : "application/json"
      },
      body :JSON.stringify(jobId)
    }).then(response => {
      return response.json();
    }).then(data=>{
      
      dispatch({
        type : UPDATE_JOB,
        payload :data
      });
    }
    );
  }
};