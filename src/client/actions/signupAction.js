import {SIGNUP_USER} from "./types";
import {uri} from "../common";
export const signupUser=(userdata)=> dispatch=>{
  
  fetch("http://"+uri+":5000/signup" ,{
    method :"POST",
    headers : {"content-type" : "application/json"},
    body :JSON.stringify(userdata)
  }).then(response => {
     
    return response.json();
  }).then(data=>dispatch({
    type : SIGNUP_USER,
    payload :data
  }));
    
};