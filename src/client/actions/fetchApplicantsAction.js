import {FETCH_APPLICANTS} from "./types";
import {uri} from "../common";
export const getApplicants=(jobId)=> dispatch=>{
  fetch("http://"+uri+":5000/getapplicants?id="+jobId ,{
    method :"GET",
    headers : {
      "content-type" : "application/json"
    }
  }).then(response => {
    return response.json();
  }).then(data=>dispatch({
    type : FETCH_APPLICANTS,
    payload :data
  })
  );   
};