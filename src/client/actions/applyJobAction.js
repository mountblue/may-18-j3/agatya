import {APPLY_JOBS} from "./types";
import {uri} from "../common";
export const applyJobs=(userData)=> dispatch=>{
  fetch("http://"+uri+":5000/applyjob" ,{
    method :"POST",
    headers : {
      "content-type" : "application/json"
    },
    body :JSON.stringify(userData)
  }).then(response => {
    return response.json();
  }).then(data=>dispatch({
    type : APPLY_JOBS,
    payload :data
  }));   
    
    
};