const GOOGLE_AUTH ="GOOGLE_AUTH";

export const googleAuth=(data)=>dispatch=>{
  dispatch({
    type:GOOGLE_AUTH,
    payload:data
  });
};