import {AUTH_USER} from "./types";
import login from "../components/login";
import {uri} from "../common";
export const authUser=(loginCredential)=> dispatch=>{
  if(!loginCredential){
    fetch("http://"+uri+":5000/login" ,{
      method :"POST",
      credentials :"include",
      headers : {
        "content-type" : "application/json"
      },
      body :JSON.stringify({})
    }).then(response => {
      return response.json();
    }).then(data=>dispatch({
      type : AUTH_USER,
      payload :data
    })
    ).catch(a=>{console.error(a);});
  }else{
    if (loginCredential.googleId) {
      dispatch({
        type: AUTH_USER,
        payload: loginCredential
      });
      return;
    }
    fetch("http://"+uri+":5000/login" ,{
    // fetch("http://"+uri+":5000/login" ,{
      method :"POST",
      credentials :"include",
      headers : {
        "content-type" : "application/json"
      },
      body :JSON.stringify(loginCredential)
    }).then(response => {
      return response.json();
    }).then(data=>dispatch({
      type : AUTH_USER,
      payload :data
    })).catch(a=>{});

    //fetch for authentication and dispatch the state of login
  } 
};
