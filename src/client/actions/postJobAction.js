import {POST_JOBS} from "./types";
import {uri} from "../common";
export const postJobs=(userdata)=> dispatch=>{
  fetch("http://"+uri+":5000/jobpost" ,{
    method :"POST",
    headers : {"content-type" : "application/json"},
    body :JSON.stringify(userdata)
  }).then(response => {
        
    return response.json();
  }).then(data=>dispatch({
    type : POST_JOBS,
    payload :data
  }));   
//fetch for authentication and dispatch the state of login
    
};