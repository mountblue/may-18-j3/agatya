import {LOGOUT} from "./types";
import {uri} from "../common";
export const logoutUser=()=>dispatch=>{
  
  fetch("http://"+uri+":5000/logout" ,{ 
    method :"GET",
    credentials:"include",
    headers : {"content-type" : "application/json"}
  }).then(response => dispatch({
    type : LOGOUT,
    payload :response
  }));
     
};