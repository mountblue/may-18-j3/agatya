import {
  UPLOAD_RESUME
} from "./types";
import login from "../components/login";
import {
  uri
} from "../common";
export const uploadResume = (url) => dispatch => {
  // console.log("resume.type",resume.type ,"file" ,resume);
  dispatch({
    type: UPLOAD_RESUME,
    payload: url
  });
  //fetch for authentication and dispatch the state of login
};