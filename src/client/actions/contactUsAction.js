import {CONTACT_US} from "./types";
import {uri} from "../common";


export const contactUs=(userData)=> dispatch=>{
  fetch("http://"+uri+":5000/contactUs" ,{
    method :"POST",
    headers : {
      "content-type" : "application/json"
    },
    body :JSON.stringify(userData)
  }).then(response => {
    return response.json();
  }).then(data=>dispatch({
    type : CONTACT_US,
    payload :data
  }));   
    
    
};