import {DELETE_JOB} from "./types";
import {uri} from "../common";
export const deleteJob=(jobId)=> dispatch=>{
  fetch("http://"+uri+":5000/deletejob?id="+jobId ,{
    method :"GET",
    headers : {
      "content-type" : "application/json"
    }
  }).then(response => {
    return response.json();
  }).then(data=>dispatch({
    type : DELETE_JOB,
    payload :data
  })
  );   
    
    
};