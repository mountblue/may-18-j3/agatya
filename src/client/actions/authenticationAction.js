import {AUTHENTICATION} from "./types";
import {uri} from "../common";
export const authentication = ()=>dispatch=>{
  if(document.cookie.length>5){
    const tokenString = document.cookie;
    const token=tokenString.substring(2,tokenString.length);
    fetch("http://"+uri+":5000/login" ,{
      method :"POST",
      credential :"include",
      headers : {
        "content-type" : "application/json"
      },
      body :JSON.stringify({token:token})
    }).then(response => {
      return response.json();
    }).then(data=>dispatch({
      type : AUTHENTICATION,
      payload :data
    })
    ).catch(a=>{});
  }  
};