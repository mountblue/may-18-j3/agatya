import {FETCH_JOBS} from "./types";
import {uri} from "../common";
export const fetchJobs=(req)=> dispatch=>{
  fetch("http://"+uri+":5000/getjob?id="+req.id ,{
    method :"GET",
    credentials:"include",
    headers : {    "content-type" : "application/json"}
  }).then(response => {

    return response.json();
  }).then(data=>dispatch({
    type : FETCH_JOBS,
    payload :data
  })); 
   
  //fetch for authentication and dispatch the state of login
    
};