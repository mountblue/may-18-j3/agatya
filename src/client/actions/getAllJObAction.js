import {FETCH_ALL_JOBS} from "./types";
import {uri} from "../common";
export const fetchAllJobs=(req)=> dispatch=>{
  const keyword= req.id?req.id:"";
  
  fetch("http://"+uri+":5000/getjob/"+keyword ,{
    method :"GET",
    headers : {"content-type" : "application/json"}
  }).then(response => {

    return response.json();
  }).then(data=>dispatch({
    type : FETCH_ALL_JOBS,
    payload :data
  })); 
   
  //fetch for authentication and dispatch the state of login
    
};