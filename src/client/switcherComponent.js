import {React ,Component} from "react";
import Login from "./components/login";
import Logo from "./components/logo";
import PostJob from "./components/postJob";
import RecruiterDash from "./components/recruiterDash";
import SignUp from "./components/signup";
import ApplyJob from "./components/applyjob";
import {connect} from "react-redux";
class Switcher extends Component{
  render(){
    return(
      <React.Fragment>
        <Logo/><Login/>
      </React.Fragment>
    );
  }  
}


export default connect(null ,{})(Switcher);